using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use

        public bool debugmode = false;

        private void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
        }


        private void FixedUpdate()
        {
            // pass the input to the car!
            if (!debugmode)
            {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Gas");
                float brake = CrossPlatformInputManager.GetAxis("Brake");
#if !MOBILE_INPUT
                if (brake < 0)
                    brake = 0;
                if (v < 0)
                    v = 0;
                //Debug.LogWarning("brake = " + brake + " gas = " + v);
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");
                m_Car.Move(h, v, -brake, handbrake);

#else
            m_Car.Move(h, v, v, 0f);
#endif
            }
            else
            {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");
                m_Car.Move(h, v, v, handbrake);

            }

        }
    }
}
